﻿using Klient.FootballerServiceRef;
using Kontrakt;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Klient
{
    public partial class Form1 : Form
    {
        FootballerServiceClient client = new FootballerServiceClient("WSHttpBinding_IFootballerService");
        FootballerServiceClient client2 = new FootballerServiceClient("WSHttpBinding_IFootballerService1");
        FootballerServiceClient client3 = new FootballerServiceClient("WSHttpBinding_IFootballerService2");
        FootballerServiceClient clientNetTcp = new FootballerServiceClient("NetTcpBinding_IFootballerService");
        FootballerServiceClient clientPipe = new FootballerServiceClient("NetNamedPipeBinding_IFootballerService");
        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            string name = txtName.Text.ToString();
            string surname = txtSurname.Text.ToString();
            int age = Convert.ToInt32(numAge.Value);
            int goals = Convert.ToInt32(numGoals.Value);
            if (name.Length > 0 && surname.Length > 0 && age > 0)
            {
                Footballer footballerToAdd = createFootballer(name, surname, age, goals);
               bool added = client.AddFootballer(footballerToAdd);
                if (added)
                {
                    richTextBox1.Text = "Pomyślnie dodano.";
                }
                else
                {
                    richTextBox1.Text = "Istnieje już piłkarz o tym nazwisku na liście.";
                }
            }
            else
            {
                richTextBox1.Text = "Wprowadź poprawne dane.";
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string surname = txtSurname.Text.ToString();
            if (surname.Length > 0)
            {
                Boolean deleted = client3.DeleteFootballerBySurname(surname);
                if (deleted)
                {
                    richTextBox1.Text = "Pomyślnie usunięto.";
                }
                else
                {
                    richTextBox1.Text = "Brak piłkarza o tym nazwisku.";
                }
            }
            else
            {
                richTextBox1.Text = "Wprowadź poprawne dane.";
            }

        }


        private void btnShowAll_Click(object sender, EventArgs e)
        {
            Footballer[] footballersList = clientNetTcp.GetAllFootballers();
            string message = "Imię:\t\tNazwisko:\tWiek:\t\tLiczba goli";
            foreach(var footballer in footballersList)
            {
                message += "\n" + footballer.Name + "\t\t" + footballer.Surname + "\t\t" + footballer.Age + "\t\t" + footballer.Goals + "\t\t";
            }
            richTextBox1.Text = message;
        }

        private void btnGoals_Click(object sender, EventArgs e)
        {
            string surname = txtSurname.Text.ToString();
            int goals = clientPipe.GetGoalsOfFootballerBySurname(surname);
            if (goals == -1){
                richTextBox1.Text = "Brak piłkarza o tym nazwisku.";
            }
            else
            {
                richTextBox1.Text = surname + " strzelił " + goals + " goli.";
            }
        }

        private void btnFootballersByAge_Click(object sender, EventArgs e)
        {
            Footballer[] footballersList = client2.GetFootballersByAge(Convert.ToInt32(numAge.Value));
            string message = "Imię:\t\tNazwisko:\tWiek:\t\tLiczba goli";
            foreach (var footballer in footballersList)
            {
                message += "\n" + footballer.Name + "\t\t" + footballer.Surname + "\t\t" + footballer.Age + "\t\t" + footballer.Goals + "\t\t";
            }
            richTextBox1.Text = message;
        }

        private Footballer createFootballer(string name, string surname, int age, int goals)
        {
            return new Footballer()
            {
                Name = name,
                Surname = surname,
                Age = age,
                Goals = goals
            };
        }
    }
}
