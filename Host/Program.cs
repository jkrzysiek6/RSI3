﻿using Kontrakt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Host
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost mojHost = new ServiceHost(typeof(FootballerService));
            Uri httpAddress1a = new Uri("http://localhost:10001/Service1/FootballerService");
            Uri httpAddress1b = new Uri("http://localhost:10001/Service1/FootballPlayerService");
            Uri httpAddress2 = new Uri("http://localhost:20001");
            string tcpAddress = "net.tcp://localhost:30001/TcpFootballerService";
            string pipeAddress = "net.pipe://localhost/PipeFootballerService";

            try
            {
                ServiceEndpoint httpEndpoint1a = mojHost.Description.Endpoints.Find(httpAddress1a);
                ServiceEndpoint httpEndpoint1b = mojHost.AddServiceEndpoint(typeof(IFootballerService), new WSHttpBinding(), httpAddress1b);
                ServiceEndpoint httpEndpoint2 = mojHost.AddServiceEndpoint(typeof(IFootballerService), new WSHttpBinding(), httpAddress2);
                ServiceEndpoint tcpEndpoint = mojHost.AddServiceEndpoint(typeof(IFootballerService), new NetTcpBinding(), tcpAddress);
                ServiceEndpoint pipeEndpoint = mojHost.AddServiceEndpoint(typeof(IFootballerService), new NetNamedPipeBinding(), pipeAddress);

                infoAboutEndpoint(httpEndpoint1a);
                infoAboutEndpoint(httpEndpoint1b);
                infoAboutEndpoint(httpEndpoint2);
                infoAboutEndpoint(tcpEndpoint);
                infoAboutEndpoint(pipeEndpoint);


                mojHost.Open();
                displayContractDescription();

                Console.WriteLine("\n*** Serwis 1 jest uruchomiony ***");
                Console.WriteLine("Nacisnij <ENTER> aby zakonczyc.");
                Console.WriteLine();
                Console.ReadLine();

                mojHost.Close();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("Wystapil wyjatek: {0}", ce.Message);
                mojHost.Abort();
            }
        }

        private static void infoAboutEndpoint(ServiceEndpoint endpoint)
        {
            Console.WriteLine("\nService endpoint {0} contains the following:", endpoint.Name);
            Console.WriteLine("Binding: {0}", endpoint.Binding.ToString());
            Console.WriteLine("Contract: {0}", endpoint.Contract.ToString());
            Console.WriteLine("ListenUri: {0}", endpoint.ListenUri.ToString());
            Console.WriteLine("ListenUriMode: {0}",
            endpoint.ListenUriMode.ToString());
        }

        private static void displayContractDescription()
        {
            ContractDescription cd = ContractDescription.GetContract(typeof(IFootballerService));
            Console.WriteLine("\nDisplaying information for contract: {0}",
            cd.Name.ToString());
            string configName = cd.ConfigurationName;
            Console.WriteLine("\tConfiguration name: {0}", configName);
            Type contractType = cd.ContractType;
            Console.WriteLine("\tContract type: {0}", contractType.ToString());
            bool hasProtectionLevel = cd.HasProtectionLevel;
            if (hasProtectionLevel)
            {
                ProtectionLevel protectionLevel = cd.ProtectionLevel;
                Console.WriteLine("\tProtection Level: {0}",
                protectionLevel.ToString());
            }
            string name = cd.Name;
            Console.WriteLine("\tName: {0}", name);
            string namespc = cd.Namespace;
            Console.WriteLine("\tNamespace: {0}", namespc);
            OperationDescriptionCollection odc = cd.Operations;
            Console.WriteLine("\tDisplay Operations:");
            foreach (OperationDescription od in odc)
            {
                Console.WriteLine("\t\t" + od.Name);
            }
            SessionMode sm = cd.SessionMode;
            Console.WriteLine("\tSessionMode: {0}", sm.ToString());

        }
    }
}
