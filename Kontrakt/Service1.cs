﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Kontrakt
{
    public class FootballerService : IFootballerService
    {
        private static List<Footballer> footballers = new List<Footballer>();
        public bool AddFootballer(Footballer footballerToAdd)
        {
            if(footballers.Count(footballer => footballer.Surname.Equals(footballerToAdd.Surname))==1)
            {
                Console.WriteLine("Istnieje już piłkarz o tym nazwisku na liście.");
                return false;
            }
            Console.WriteLine("Dodano piłkarza: " + footballerToAdd.Name + " " + footballerToAdd.Surname);
            footballers.Add(footballerToAdd);
            return true;
        }

        public Boolean DeleteFootballerBySurname(string surname)
        {
            foreach (var footballer in footballers)
            {
                if (footballer.Surname.Equals(surname))
                {
                    footballers.Remove(footballer);
                    Console.WriteLine("Pomyślnie usunięto piłkarza.");
                    return true;
                }
            }
             Console.WriteLine("Brak piłkarza o nazwisku: " + surname);
            return false;
        }

        public List<Footballer> GetAllFootballers()
        {
            Console.WriteLine("Wyświetlono wszystkich piłkarzy.");
            return footballers;
        }

        public List<Footballer> GetFootballersByAge(int age)
        {
            List<Footballer> filteredFootballers = new List<Footballer>();
            foreach (var footballer in footballers)
            {
                if (footballer.Age.Equals(age))
                {
                    filteredFootballers.Add(footballer);
                }
            }
            Console.WriteLine("Wyświetlono piłkarzy o wieku: "+age);
            return filteredFootballers;
        }

        public int GetGoalsOfFootballerBySurname(string surname)
        {
            foreach (var footballer in footballers)
            {
                if (footballer.Surname.Equals(surname))
                {
                    Console.WriteLine(surname + " strzelił " + footballer.Goals + " goli.");
                    return footballer.Goals;
                }
            }
            Console.WriteLine("Brak piłkarza o tym nazwisku.");
            return -1;
        }
    }
}
