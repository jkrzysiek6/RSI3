﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Kontrakt
{
    [ServiceContract]
    public interface IFootballerService
    {
        [OperationContract]
        bool AddFootballer(Footballer footballerToAdd);

        [OperationContract]
        Boolean DeleteFootballerBySurname(string surname);

        [OperationContract]
        List<Footballer> GetFootballersByAge(int age);

        [OperationContract]
        List<Footballer> GetAllFootballers();

        [OperationContract]
        int GetGoalsOfFootballerBySurname(string surname);
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "Kontrakt.ContractType".
    [DataContract]
    public class Footballer
    {
        [DataMember]
        private string name;

        [DataMember]
        private string surname;

        [DataMember]
        private int age;

        [DataMember]
        private int goals;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public int Goals
        {
            get { return goals; }
            set { goals = value; }
        }

        public string ToSring()
        {
            return name + " " + surname + ", wiek: " + age + "gole: " + goals;
        }
    }
}
